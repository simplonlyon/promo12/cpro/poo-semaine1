<?php

namespace App\Tests\Solver;

use PHPUnit\Framework\TestCase;
use App\Solver\TicTacToe;

class TicTacToeTests extends TestCase {

    /**
     * @var TicTacToe
     */
    private $solver;

    public function setUp(): void {
        $this->solver = new TicTacToe();
    }

    public function testFirstRowO() {
        $grid = [
            ['O', 'O', ''],
            ['', 'X', ''],
            ['', '', 'X'],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', 'O', 'O'],
            ['', 'X', ''],
            ['', '', 'X'],
        ], $solution);
    }

    public function testFirstRowMiddleO() {
        $grid = [
            ['O', '', 'O'],
            ['', '', 'X'],
            ['', '', 'X'],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', 'O', 'O'],
            ['', '', 'X'],
            ['', '', 'X'],
        ], $solution);
    }

    public function testFirstRowMiddleX() {
        $grid = [
            ['X', '', 'X'],
            ['', '', 'O'],
            ['', '', 'O'],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['X', 'X', 'X'],
            ['', '', 'O'],
            ['', '', 'O'],
        ], $solution);
    }

    public function testThirdRowMiddleX() {
        $grid = [
            ['', '', 'O'],
            ['', '', 'O'],
            ['X', '', 'X'],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['', '', 'O'],
            ['', '', 'O'],
            ['X', 'X', 'X'],
        ], $solution);
    }

    public function testFirstColO() {
        $grid = [
            ['O', 'X', 'X'],
            ['O', '', ''],
            ['', '', ''],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', 'X', 'X'],
            ['O', '', ''],
            ['O', '', ''],
        ], $solution);
    }

    public function testFirstColMiddleO() {
        $grid = [
            ['O', 'X', 'X'],
            ['', '', ''],
            ['O', '', ''],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', 'X', 'X'],
            ['O', '', ''],
            ['O', '', ''],
        ], $solution);
    }
    public function testMiddleColMiddleX() {
        $grid = [
            ['O', 'X', ''],
            ['', 'X', 'O'],
            ['', '', ''],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', 'X', ''],
            ['', 'X', 'O'],
            ['', 'X', ''],
        ], $solution);
    }
    public function testThirdColX() {
        $grid = [
            ['O', '', ''],
            ['', 'O', 'X'],
            ['', '', 'X'],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', '', 'X'],
            ['', 'O', 'X'],
            ['', '', 'X'],
        ], $solution);
    }
    public function testFirstDiagO() {
        $grid = [
            ['O', 'X', ''],
            ['', 'O', 'X'],
            ['', '', ''],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', 'X', ''],
            ['', 'O', 'X'],
            ['', '', 'O'],
        ], $solution);
    }
    
    public function testFirstDiagMiddleO() {
        $grid = [
            ['O', 'X', ''],
            ['', '', 'X'],
            ['', '', 'O'],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['O', 'X', ''],
            ['', 'O', 'X'],
            ['', '', 'O'],
        ], $solution);
    }
    public function testSecondDiagX() {
        $grid = [
            ['', 'O', ''],
            ['O', 'X', ''],
            ['X', '', ''],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['', 'O', 'X'],
            ['O', 'X', ''],
            ['X', '', ''],
        ], $solution);
    }
    
    public function testSecondDiagMiddleX() {
        $grid = [
            ['', 'O', 'X'],
            ['O', '', ''],
            ['X', '', ''],
        ];
        $solution = $this->solver->solve($grid);
        $this->assertEquals([
            ['', 'O', 'X'],
            ['O', 'X', ''],
            ['X', '', ''],
        ], $solution);
    }
   
}