<?php

namespace App\Solver;

class Sudoku
{


    public function solve(array $grid): array
    {
        // $again = false;

        //version avec boucle plutôt que récursivité
        $again = true;
        while ($again) {
            $again = false;

            foreach ($grid as $rowKey => $row) {
                foreach ($row as $colKey => $col) {
                    if (is_null($col)) {
                        $solutions = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                        $temp = $row;
                        foreach ($grid as $colValue) {
                            $temp[] = $colValue[$colKey];
                        }

                        $rowStart = $rowKey - $rowKey % 3;
                        $colStart = $colKey - $colKey % 3;

                        for ($x = $rowStart; $x < $rowStart + 3; $x++) {
                            for ($y = $colStart; $y < $colStart + 3; $y++) {
                                $temp[] = $grid[$x][$y];
                            }
                        }

                        $solutions = array_diff($solutions, $temp);

                        if (count($solutions) == 1) {
                            $grid[$rowKey][$colKey] = array_sum($solutions);
                        } else {
                            $again = true;
                        }
                    }
                }
            }
        }

        //version avec récursivité
        // if($again) {
        //     $grid = $this->solve($grid);
        // }

        return $grid;
    }
}
