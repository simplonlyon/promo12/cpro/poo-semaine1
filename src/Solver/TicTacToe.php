<?php

namespace App\Solver;

class TicTacToe {

   /*public function solve(array $grid):array {

        $count = 0;
        $symbol = null;
        $empty = null;
        foreach($grid[0] as $key => $value) {
            if($value != '') {
                $symbol = $value;
                $count++;
            }
            if()
            if($value == 'O') {
                $count++;
            } else {
                $empty = $key;
            }
        }

        if($count == 2) {
            $grid[0][$empty] = 'O';
        }

        return $grid;
    }*/
     public function solve(array $grid):array {
        $symbols = ['O', 'X'];
        foreach($grid as $rowKey => $line) {

            foreach($line as $key => $value) {
                

                if($value == '') {

                    foreach($symbols as $symbol) {
                        $gridCopy = $grid;

                        $gridCopy[$rowKey][$key] = $symbol;
                        
                        if($this->winningLine($gridCopy[$rowKey]) || $this->winningCol($key, $gridCopy) || $this->winningDiags($gridCopy))  {
                            
                            return $gridCopy;
                        }

                    }
                    
                }
            }

        }
        return $grid;
    }

    private function winningLine(array $line):bool {
        return $line[0] != '' && $line[0] == $line[1] && $line[0] == $line[2];
    }

    private function winningCol(int $colKey, array $grid):bool {
        
        return $grid[0][$colKey] != '' && $grid[0][$colKey] == $grid[1][$colKey] && $grid[1][$colKey] == $grid[2][$colKey];
    }

    private function winningDiags(array $grid):bool {
        return ($grid[0][0] != '' && $grid[0][0] == $grid[1][1] && $grid[1][1] == $grid[2][2]) ||
        ($grid[0][2] != '' && $grid[0][2] == $grid[1][1] && $grid[1][1] == $grid[2][0]) ;
    }

}