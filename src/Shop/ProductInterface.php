<?php

namespace App\Shop;

interface ProductInterface {
    function getPrice():float;
    function isAvailable():bool;
    function getLabel():string;
    function isSame(ProductInterface $product):bool;
    function incrementStock():void;
    function decrementStock():void;
}