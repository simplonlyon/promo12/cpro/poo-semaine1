<?php

namespace App\Shop;

interface WoodType {
    const OAK = 'Oak';
    const BASIC = 'Basic';
    const CHIPBOARD = 'Chipboard';
}